# Makefile for JS Parser

# Variables
APP_NAME = js-parser
BUILD_DIR = build

# Build target
build:
	@echo "Building $(APP_NAME)..."
	docker compose build

# Start target
run:
	@echo "Starting $(APP_NAME)..."
	docker compose up -d

# Stop target
stop:
	@echo "Stopping $(APP_NAME)..."
	docker compose stop

# Restart target
restart: stop build run

# Status target
status:
	docker compose ps

# Help target
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@echo "  build    Build the $(APP_NAME) image"
	@echo "  run      Start the $(APP_NAME) container"
	@echo "  stop     Stop the $(APP_NAME) container"
	@echo "  restart  Restart the $(APP_NAME) container"
	@echo "  status   Show the status of the $(APP_NAME) container"
	@echo "  logs     Show the logs of the $(APP_NAME) container"
	@echo "  help     Show this help message"

# Show logs
logs:
	docker compose logs -f

# Default target
.DEFAULT_GOAL := help