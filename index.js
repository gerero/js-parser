const express = require('express')
const morgan = require('morgan')
const jsdom = require("jsdom")
const { JSDOM } = jsdom

const App = require('./modules/app.js')

class Server {
  constructor() {
    this.linkMatrix
    this.app = new App()
    this.router = express()
    this.port = 3000
    this.router.use([express.json(), morgan('dev')])
    this.setUpRoutes()
  }

  start() {
    this.router.listen(this.port, () => {
      console.log(`Server is running on port ${this.port}`)
    })
  }

  stop() {
    this.router.close(() => {
      console.log('Server has been stopped')
    })
  }

  //async function to announce the server methods
  async setUpRoutes() {

    // get method to get the data from the google sheet
    this.router.get('/get', async (req, res) => {
      this.linkMatrix = await this.app.fetchData({url: this.app.url})

      return res.status(200).json(this.linkMatrix)
      // return res.status(200).json(this.app.returnConfig())
    })
  }

  convertPriceToNumber(price) {
    return parseFloat(price.replace(/[^0-9.]/g, ''))
  }

}

const server = new Server()
server.start()

process.on('SIGINT', async () => {
  console.log("Stopping app")
  await server.stop()
  process.exit()
})

process.on('SIGTERM', async () => {
  console.log("Stopping app")
  await server.stop()
  process.exit()
})