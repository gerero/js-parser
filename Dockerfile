FROM node:alpine
USER node
WORKDIR /app
COPY package.json .
RUN npm install --silent
EXPOSE 3000
