const axios = require('axios')
const https = require('https')

class App {
  constructor() {
    this.url = "https://script.google.com/macros/s/AKfycbwHAjZBzb9DdhyRUyhPmVDACxwOwwyDR60WWItJ6ID_x2qXHIBh-k5ks9Q2A7BPn5uIow/exec"
    this.instance = axios.create({
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
        keepAlive: true
      }),
      headers: { 'Accept-Encoding': 'gzip' }
    })
    this.config = {
      "htps://optom-k.com/": {
        name: '.item-info__item-title',
        price: '.item-info__price>b>span'
      },
      "https://ua.color-it.ua/": {
        name: '.style_heading__2WDSS.heading',
        price: '.style_price__2EdnY.style_withOld__3gpkg>div'
      }
    }
  }

  async fetchData(_params) {
    let { url } = _params
    try {
      const response = await this.instance.get(url)
      return response.data
    } catch (error) {
      console.error(`Failed to fetch data from ${url}:`, error)
      throw error
    }
  }

  returnConfig() {
    return this.config['htps://optom-k.com/']
  }

  async convertDomToJson(_params) {
    const { url, document } = _params
    let data = {}

    if (url === "htps://optom-k.com/") {
      data = {
        name: document.querySelector('.item-info__item-title').textContent,
        price: document.querySelector('.item-info__price>b>span').textContent
      }
    } else if (url === 'https://ua.color-it.ua/') {
      data = {
        name: document.querySelector('.style_heading__2WDSS.heading').textContent,
        price: document.querySelector('.style_price__2EdnY.style_withOld__3gpkg>div').textContent
      }
    }

    return data
  }

  convertPriceToNumber(price) {
    return parseFloat(price.replace(/[^0-9.]/g, ''))
  }

}

module.exports = App